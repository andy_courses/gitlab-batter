// Dependencies
import React, {Component} from 'react';

// Assets


class Avatar extends Component {
    render() {
        return (
            <div>
                <ProfilePic username={this.props.username} />
                <ProfileLink username={this.props.username} />
            </div>
        );
    }
}

export default Avatar;