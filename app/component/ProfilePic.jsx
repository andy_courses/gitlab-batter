// Dependencies
import React, {Component} from 'react';

// Assets


class ProfilePic extends Component {
    render() {
        return (
            <img src={'https://photo.fb.com/' + this.props.username} />
        );
    }
}

export default ProfilePic;