// Dependencies
var React = require('react');
var PropTypes = require('prop-types');

// Assets


class SelectLanguage extends Component {


    render() {

        var languages =  ["All", "JavaScrip", "Ruby", "Java", "CSS", "Python"];

        return (
            <ul className="languages">

                {languages.map((lang) => {
                    console.log("Down here", this);
                    return (
                        <li
                            style={lang === this.props.selectedLanguage ? {color: '#d0021b'} : null}
                            onClick={this.props.onSelect.bind(null, lang)}
                            key={lang}>{lang}</li>
                    )
                })}
            </ul>
        );
    }
}

SelectLanguage.propTypes = {
    selectedLanguage: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired,

};

module.exports = SelectLanguage;